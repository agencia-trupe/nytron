<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('catalogo', 'App\Models\Produto');
		$router->model('veiculo', 'App\Models\Veiculo');
		$router->model('marca', 'App\Models\Marca');
		$router->model('linha', 'App\Models\Linha');
		$router->model('categoria', 'App\Models\Categoria');
		$router->model('produtos-apresentacao', 'App\Models\ProdutosApresentacao');
		$router->model('catalogos', 'App\Models\Catalogos');
		$router->model('banners', 'App\Models\Banner');
		$router->model('equipe-texto', 'App\Models\EquipeTexto');
		$router->model('equipe', 'App\Models\Equipe');
		$router->model('assistencia-tecnica', 'App\Models\AssistenciaTecnica');
		$router->model('tecnicos', 'App\Models\Tecnico');
		$router->model('empresa', 'App\Models\Empresa');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
