<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Marca extends Model
{
    protected $table = 'marca';

    protected $guarded = ['id'];

    public $timestamps = false;
}
