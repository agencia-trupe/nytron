<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Categoria extends Model
{
    protected $table = 'categoria';

    protected $guarded = ['id'];

    public $timestamps = false;
}
