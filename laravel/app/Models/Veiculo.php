<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Veiculo extends Model
{
    protected $table = 'veiculo';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function marca()
    {
        return $this->belongsTo(Marca::class, 'id_marca');
    }

    public function linha()
    {
        return $this->belongsTo(Linha::class, 'id_linha');
    }
}
