<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class AssistenciaTecnica extends Model
{
    protected $table = 'assistencia_tecnica';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 465,
            'height' => null,
            'path'   => 'assets/img/assistencia-tecnica/'
        ]);
    }
}
