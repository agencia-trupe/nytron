<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Catalogos extends Model
{
    protected $table = 'catalogos';

    protected $guarded = ['id'];

    public static function upload_imagem_tensionadores_e_polias()
    {
        return CropImage::make('imagem_tensionadores_e_polias', [
            'width'  => 185,
            'height' => 250,
            'path'   => 'assets/img/catalogos/'
        ]);
    }

    public static function upload_imagem_kits_de_distribuicao()
    {
        return CropImage::make('imagem_kits_de_distribuicao', [
            'width'  => 185,
            'height' => 250,
            'path'   => 'assets/img/catalogos/'
        ]);
    }

}
