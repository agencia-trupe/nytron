<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class EquipeTexto extends Model
{
    protected $table = 'equipe_texto';

    protected $guarded = ['id'];

}
