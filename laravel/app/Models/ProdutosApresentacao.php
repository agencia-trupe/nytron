<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutosApresentacao extends Model
{
    protected $table = 'produtos_apresentacao';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 570,
            'height' => null,
            'path'   => 'assets/img/produtos/apresentacao/'
        ]);
    }

}
