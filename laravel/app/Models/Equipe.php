<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;
use App\Helpers\Tools;

class Equipe extends Model
{
    protected $table = 'equipe';

    protected $guarded = ['id'];

    public function getEstadoExtensoAttribute()
    {
        return Tools::selectEstados()[$this->estado];
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
