<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Linha extends Model
{
    protected $table = 'linha';

    protected $guarded = ['id'];

    public $timestamps = false;
}
