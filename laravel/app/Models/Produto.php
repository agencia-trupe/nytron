<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Produto extends Model
{
    protected $table = 'catalogo';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function marca()
    {
        return $this->belongsTo(Marca::class, 'id_marca');
    }

    public function categoria()
    {
        return $this->belongsToMany(Categoria::class, 'catalogo_categoria', 'id_catalogo', 'id_categoria');
    }

    public function veiculo()
    {
        return $this->belongsToMany(Veiculo::class, 'catalogo_veiculo', 'id_catalogo', 'id_veiculo');
    }

    public static function upload_foto_1()
    {
        return CropImage::make('foto1', [
            'width'  => 960,
            'height' => 720,
            'upsize' => true,
            'path'   => 'assets/img/catalogo/'
        ]);
    }

    public static function upload_foto_2()
    {
        return CropImage::make('foto2', [
            'width'  => 960,
            'height' => 720,
            'upsize' => true,
            'path'   => 'assets/img/catalogo/'
        ]);
    }
}
