<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CatalogosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem_tensionadores_e_polias' => 'image',
            'arquivo_tensionadores_e_polias' => 'mimes:pdf',
            'imagem_kits_de_distribuicao' => 'image',
            'arquivo_kits_de_distruibuicao' => 'mimes:pdf',
            'arquivo_banners' => 'mimes:pdf',
        ];
    }
}
