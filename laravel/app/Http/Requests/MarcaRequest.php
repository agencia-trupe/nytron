<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MarcaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'descricao' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
