<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VeiculoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'descricao' => 'required',
            'id_marca' => 'required',
            'id_linha' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'id_marca' => 'marca',
            'id_linha' => 'linha',
        ];
    }
}
