<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosApresentacaoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto' => 'required',
            'imagem' => 'image',
            'kits_de_distribuicao' => 'required',
            'tensionadores_e_polias' => 'required',
        ];
    }
}
