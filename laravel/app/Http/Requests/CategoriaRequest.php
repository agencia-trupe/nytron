<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CategoriaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'descricao' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
