<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EquipeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'estado' => 'required',
            'nome' => 'required',
            'cargo' => '',
            'e_mail' => 'email',
            'telefone_fixo' => '',
            'telefone_celular' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
