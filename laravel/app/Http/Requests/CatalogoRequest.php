<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CatalogoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'descricao' => 'required',
            'cd_nytron' => 'required',
            'cd_original' => '',
            'aplicacao' => '',
            'refil' => '',
            'foto_1' => 'image',
            'foto_2' => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['foto_1'] = 'image';
            $rules['foto_2'] = 'image';
        }

        return $rules;
    }
}
