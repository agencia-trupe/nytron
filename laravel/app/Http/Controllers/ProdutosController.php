<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ProdutosApresentacao;
use App\Models\Catalogos;
use App\Models\Produto;
use App\Models\Linha;
use App\Models\Marca;
use App\Models\Categoria;
use App\Models\Veiculo;

class ProdutosController extends Controller
{
    public function __construct()
    {
        view()->share('linhas', Linha::lists('descricao', 'id'));
        view()->share('marcas', Marca::lists('descricao', 'id'));
        view()->share('categorias', Categoria::lists('descricao', 'id'));
    }

    public function index()
    {
        $apresentacao = ProdutosApresentacao::first();

        view()->share('catalogos', Catalogos::first());

        $produtos = Produto::orderByRaw("RAND()")->take(8)->get();

        return view('frontend.produtos.index', compact('apresentacao', 'produtos'));
    }

    public function busca()
    {
        if (request('montadora') && Marca::find(request('montadora'))) {
            view()->share('veiculos', Veiculo::where('id_marca', request('montadora'))->lists('descricao', 'id'));
        }

        if (empty(array_filter(request()->all()))) {
            return redirect()->route('produtos');
        }

        $produtos = new Produto;

        if (request('geral')) {
            $produtos = $produtos
                ->where('cd_original', 'LIKE', '%'.request('geral').'%')
                ->orWhere('cd_nytron', 'LIKE', '%'.request('geral').'%')
                ->orWhere('aplicacao', 'LIKE', '%'.request('geral').'%')
                ->orWhere('descricao', 'LIKE', '%'.request('geral').'%')
                ->orWhereHas('categoria', function($q) {
                    $q->where('descricao', 'LIKE', '%'.request('geral').'%');
                })
                ->orWhereHas('veiculo', function($q) {
                    $q->where('descricao', 'LIKE', '%'.request('geral').'%');
                })
                ->orWhereHas('marca', function($q) {
                    $q->where('descricao', 'LIKE', '%'.request('geral').'%');
                });
        };
        if (request('cd_original')) {
            $produtos = $produtos->where('cd_original', 'LIKE', '%'.request('cd_original').'%');
        }
        if (request('cd_nytron')) {
            $produtos = $produtos->where('cd_nytron', 'LIKE', '%'.request('cd_nytron').'%');
        }

        if (request('tipo_veiculo')) {
            $produtos = $produtos->whereHas('veiculo', function($query) {
                $query->where('id_linha', request('tipo_veiculo'));
            });
        }
        if (request('montadora')) {
            $produtos = $produtos->whereHas('veiculo', function($query) {
                $query->where('id_marca', request('montadora'));
            });
        }
        if (request('modelo_veiculo')) {
            $produtos = $produtos->whereHas('veiculo', function($query) {
                $query->where('id', request('modelo_veiculo'));
            });
        }
        if (request('produto')) {
            $produtos = $produtos->whereHas('categoria', function($query) {
                $query->where('id', request('produto'));
            });
        }

        $produtos = $produtos->get();

        return view('frontend.produtos.resultados', compact('produtos'));
    }

    public function show($id)
    {
        $produto = Produto::findOrFail($id);

        return view('frontend.produtos.show', compact('produto'));
    }

    public function veiculos($marca)
    {
        return Veiculo::where('id_marca', $marca->id)->lists('descricao', 'id');
    }
}
