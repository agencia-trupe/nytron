<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Catalogos;
use App\Models\Produto;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $catalogos = Catalogos::first();
        $produtos = Produto::orderByRaw("RAND()")->take(8)->get();

        return view('frontend.home', compact('banners', 'catalogos', 'produtos'));
    }
}
