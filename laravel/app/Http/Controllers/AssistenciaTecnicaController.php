<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\AssistenciaTecnica;
use App\Models\Tecnico;

class AssistenciaTecnicaController extends Controller
{
    public function index()
    {
        $assistenciaTecnica = AssistenciaTecnica::first();
        $tecnicos = Tecnico::ordenados()->get();

        return view('frontend.assistencia-tecnica', compact('assistenciaTecnica', 'tecnicos'));
    }
}
