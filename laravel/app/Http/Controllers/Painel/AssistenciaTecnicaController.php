<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AssistenciaTecnicaRequest;
use App\Http\Controllers\Controller;

use App\Models\AssistenciaTecnica;

class AssistenciaTecnicaController extends Controller
{
    public function index()
    {
        $registro = AssistenciaTecnica::first();

        return view('painel.assistencia-tecnica.edit', compact('registro'));
    }

    public function update(AssistenciaTecnicaRequest $request, AssistenciaTecnica $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = AssistenciaTecnica::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.assistencia-tecnica.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
