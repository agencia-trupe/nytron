<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CategoriaRequest;
use App\Http\Controllers\Controller;

use App\Models\Categoria;

class CategoriaController extends Controller
{
    public function index()
    {
        $registros = Categoria::get();

        return view('painel.categoria.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.categoria.create');
    }

    public function store(CategoriaRequest $request)
    {
        try {

            $input = $request->all();

            Categoria::create($input);

            return redirect()->route('painel.categoria.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Categoria $registro)
    {
        return view('painel.categoria.edit', compact('registro'));
    }

    public function update(CategoriaRequest $request, Categoria $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.categoria.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Categoria $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.categoria.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
