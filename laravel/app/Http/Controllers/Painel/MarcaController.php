<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MarcaRequest;
use App\Http\Controllers\Controller;

use App\Models\Marca;
use App\Models\Veiculo;

class MarcaController extends Controller
{
    public function index()
    {
        $registros = Marca::get();

        return view('painel.marca.index', compact('registros'));
    }

    public function show(Marca $registro)
    {
        return Veiculo::where('id_marca', $registro->id)->lists('descricao', 'id');
    }

    public function create()
    {
        return view('painel.marca.create');
    }

    public function store(MarcaRequest $request)
    {
        try {

            $input = $request->all();

            Marca::create($input);

            return redirect()->route('painel.marca.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Marca $registro)
    {
        return view('painel.marca.edit', compact('registro'));
    }

    public function update(MarcaRequest $request, Marca $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.marca.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Marca $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.marca.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
