<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CatalogoRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\Marca;
use App\Models\Categoria;
use App\Models\Veiculo;

class CatalogoController extends Controller
{
    public function index()
    {
        $registros = Produto::paginate(20);

        return view('painel.catalogo.index', compact('registros'));
    }

    public function create()
    {
        $categorias = Categoria::lists('descricao', 'id');
        $marcas     = Marca::lists('descricao', 'id');
        $veiculos   = [];

        return view('painel.catalogo.create', compact('categorias', 'marcas', 'veiculos'));
    }

    public function store(CatalogoRequest $request)
    {
        try {

            $input = $request->except('categoria', 'veiculo');

            if (! $request->has('id_marca')) {
                $input['id_marca'] = null;
            }

            if (isset($input['foto1'])) $input['foto1'] = Produto::upload_foto_1();

            if (isset($input['foto2'])) $input['foto2'] = Produto::upload_foto_2();

            $registro = Produto::create($input);

            $categoria = ($request->get('categoria') ?: []);
            $registro->categoria()->sync($categoria);

            $veiculo = ($request->get('veiculo') ?: []);
            $registro->veiculo()->sync($veiculo);

            return redirect()->route('painel.catalogo.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $registro)
    {
        $categorias = Categoria::lists('descricao', 'id');
        $marcas     = Marca::lists('descricao', 'id');
        $veiculos   = Veiculo::where('id_marca', $registro->id_marca)->lists('descricao', 'id');

        return view('painel.catalogo.edit', compact('registro', 'categorias', 'marcas', 'veiculos'));
    }

    public function update(CatalogoRequest $request, Produto $registro)
    {
        try {

            $input = $request->except('categoria', 'veiculo');

            if (! $request->has('id_marca')) {
                $input['id_marca'] = null;
            }

            if (isset($input['foto1'])) $input['foto1'] = Produto::upload_foto_1();

            if (isset($input['foto2'])) $input['foto2'] = Produto::upload_foto_2();

            $registro->update($input);

            $categoria = ($request->get('categoria') ?: []);
            $registro->categoria()->sync($categoria);

            $veiculo = ($request->get('veiculo') ?: []);
            $registro->veiculo()->sync($veiculo);

            return redirect()->route('painel.catalogo.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.catalogo.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
