<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CatalogosRequest;
use App\Http\Controllers\Controller;

use App\Models\Catalogos;

use App\Helpers\Tools;

class CatalogosController extends Controller
{
    public function index()
    {
        $registro = Catalogos::first();

        return view('painel.catalogos.edit', compact('registro'));
    }

    public function update(CatalogosRequest $request, Catalogos $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_tensionadores_e_polias'])) $input['imagem_tensionadores_e_polias'] = Catalogos::upload_imagem_tensionadores_e_polias();
            if (isset($input['imagem_kits_de_distribuicao'])) $input['imagem_kits_de_distribuicao'] = Catalogos::upload_imagem_kits_de_distribuicao();

            if ($request->hasFile('arquivo_tensionadores_e_polias')) $input['arquivo_tensionadores_e_polias'] = Tools::fileUpload('arquivo_tensionadores_e_polias');
            if ($request->hasFile('arquivo_kits_de_distribuicao')) $input['arquivo_kits_de_distribuicao'] = Tools::fileUpload('arquivo_kits_de_distribuicao');
            if ($request->hasFile('arquivo_banners')) $input['arquivo_banners'] = Tools::fileUpload('arquivo_banners');

            $registro->update($input);

            return redirect()->route('painel.catalogos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
