<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\LinhaRequest;
use App\Http\Controllers\Controller;

use App\Models\Linha;

class LinhaController extends Controller
{
    public function index()
    {
        $registros = Linha::get();

        return view('painel.linha.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.linha.create');
    }

    public function store(LinhaRequest $request)
    {
        try {

            $input = $request->all();

            Linha::create($input);

            return redirect()->route('painel.linha.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Linha $registro)
    {
        return view('painel.linha.edit', compact('registro'));
    }

    public function update(LinhaRequest $request, Linha $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.linha.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Linha $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.linha.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
