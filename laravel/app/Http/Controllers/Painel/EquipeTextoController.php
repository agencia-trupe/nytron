<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EquipeTextoRequest;
use App\Http\Controllers\Controller;

use App\Models\EquipeTexto;

class EquipeTextoController extends Controller
{
    public function index()
    {
        $registro = EquipeTexto::first();

        return view('painel.equipe-texto.edit', compact('registro'));
    }

    public function update(EquipeTextoRequest $request, EquipeTexto $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.equipe-texto.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
