<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TecnicosRequest;
use App\Http\Controllers\Controller;

use App\Models\Tecnico;

class TecnicosController extends Controller
{
    public function index()
    {
        $registros = Tecnico::ordenados()->get();

        return view('painel.tecnicos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.tecnicos.create');
    }

    public function store(TecnicosRequest $request)
    {
        try {

            $input = $request->all();

            Tecnico::create($input);

            return redirect()->route('painel.tecnicos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Tecnico $registro)
    {
        return view('painel.tecnicos.edit', compact('registro'));
    }

    public function update(TecnicosRequest $request, Tecnico $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.tecnicos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Tecnico $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.tecnicos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
