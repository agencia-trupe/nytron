<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VeiculoRequest;
use App\Http\Controllers\Controller;

use App\Models\Veiculo;
use App\Models\Marca;
use App\Models\Linha;

class VeiculoController extends Controller
{
    public function index()
    {
        if (request('filtro')) {
            $registros = Veiculo::
                where('descricao', 'LIKE', '%'.request('filtro').'%')
                ->paginate(20);
        } else {
            $registros = Veiculo::paginate(20);
        }

        return view('painel.veiculo.index', compact('registros'));
    }

    public function create()
    {
        $marcas = Marca::lists('descricao', 'id');
        $linhas = Linha::lists('descricao', 'id');

        return view('painel.veiculo.create', compact('marcas', 'linhas'));
    }

    public function store(VeiculoRequest $request)
    {
        try {

            $input = $request->all();

            Veiculo::create($input);

            return redirect()->route('painel.veiculo.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Veiculo $registro)
    {
        $marcas = Marca::lists('descricao', 'id');
        $linhas = Linha::lists('descricao', 'id');

        return view('painel.veiculo.edit', compact('registro', 'marcas', 'linhas'));
    }

    public function update(VeiculoRequest $request, Veiculo $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.veiculo.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Veiculo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.veiculo.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
