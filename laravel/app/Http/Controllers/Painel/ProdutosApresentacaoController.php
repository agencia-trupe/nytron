<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosApresentacaoRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutosApresentacao;

class ProdutosApresentacaoController extends Controller
{
    public function index()
    {
        $registro = ProdutosApresentacao::first();

        return view('painel.produtos-apresentacao.edit', compact('registro'));
    }

    public function update(ProdutosApresentacaoRequest $request, ProdutosApresentacao $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ProdutosApresentacao::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.produtos-apresentacao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
