<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Equipe;
use App\Models\EquipeTexto;

use App\Helpers\Tools;

class EquipeController extends Controller
{
    public function index()
    {
        $texto  = EquipeTexto::first();
        $equipe = Equipe::ordenados()->get()
            ->sortBy('estadoExtenso')
            ->groupBy('estado');

        if (request('busca')) {
            if (! array_key_exists(strtoupper(request('busca')), Tools::selectEstados())) {
                abort('404');
            }

            $equipe = Equipe::where('estado', strtoupper(request('busca')))
                ->ordenados()->get();
        }

        return view('frontend.equipe', compact('texto', 'equipe'));
    }
}
