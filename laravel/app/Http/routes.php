<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresa', 'EmpresaController@index')->name('empresa');
    Route::get('produtos', 'ProdutosController@index')->name('produtos');
    Route::get('produtos/busca', 'ProdutosController@busca')->name('produtos.busca');
    Route::get('produtos/{produto}', 'ProdutosController@show')->name('produtos.show');
    Route::get('veiculos/{marca}', 'ProdutosController@veiculos')->name('produtos.veiculos');
    Route::get('equipe', 'EquipeController@index')->name('equipe');
    Route::get('assistencia-tecnica', 'AssistenciaTecnicaController@index')->name('assistencia-tecnica');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('catalogo', 'CatalogoController');
		Route::resource('veiculo', 'VeiculoController');
		Route::resource('marca', 'MarcaController');
		Route::resource('linha', 'LinhaController');
		Route::resource('categoria', 'CategoriaController');
		Route::resource('produtos-apresentacao', 'ProdutosApresentacaoController', ['only' => ['index', 'update']]);
		Route::resource('catalogos', 'CatalogosController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
		Route::resource('equipe-texto', 'EquipeTextoController', ['only' => ['index', 'update']]);
		Route::resource('equipe', 'EquipeController');
		Route::resource('assistencia-tecnica', 'AssistenciaTecnicaController', ['only' => ['index', 'update']]);
		Route::resource('tecnicos', 'TecnicosController');
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
