<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email' => 'sac@dayco.com',
            'telefone_comercial' => '+55 11 3146 4770',
            'endereco_comercial' => 'Av. Henry Ford, 867<br>Mooca - São Paulo - SP<br>03109-000',
            'telefone_fabrica' => '19 3813-9730',
            'endereco_fabrica' => 'Rua Silvio Galizoni, 67-273<br>Cubatão - Itapira - SP<br>13972-340',
            'facebook' => 'https://www.facebook.com/nytronindustria/',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.8548602265064!2d-46.60104818437808!3d-23.57365566793298!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59561c9d7309%3A0xfafbe25ac848975d!2sAv.+Henry+Ford%2C+867+-+Parque+da+Mooca%2C+S%C3%A3o+Paulo+-+SP%2C+03107-000!5e0!3m2!1spt-BR!2sbr!4v1540615670891" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>'
        ]);
    }
}
