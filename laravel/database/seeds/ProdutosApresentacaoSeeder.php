<?php

use Illuminate\Database\Seeder;

class ProdutosApresentacaoSeeder extends Seeder
{
    public function run()
    {
        DB::table('produtos_apresentacao')->insert([
            'texto' => '',
            'imagem' => '',
            'kits_de_distribuicao' => '',
            'tensionadores_e_polias' => '',
        ]);
    }
}
