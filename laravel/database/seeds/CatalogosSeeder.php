<?php

use Illuminate\Database\Seeder;

class CatalogosSeeder extends Seeder
{
    public function run()
    {
        DB::table('catalogos')->insert([
            'imagem_tensionadores_e_polias' => '',
            'arquivo_tensionadores_e_polias' => '',
            'imagem_kits_de_distribuicao' => '',
            'arquivo_kits_de_distribuicao' => '',
            'arquivo_banners' => '',
        ]);
    }
}
