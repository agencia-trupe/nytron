<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinhaTable extends Migration
{
    public function up()
    {
        Schema::create('linha', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('linha');
    }
}
