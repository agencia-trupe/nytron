<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogoTable extends Migration
{
    public function up()
    {
        Schema::create('catalogo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao');
            $table->string('cd_nytron');
            $table->string('cd_original');
            $table->text('aplicacao');
            $table->string('refil');
            $table->integer('id_marca');
            $table->string('foto1');
            $table->string('foto2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('catalogo');
    }
}
