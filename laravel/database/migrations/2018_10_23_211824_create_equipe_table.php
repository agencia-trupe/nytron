<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipeTable extends Migration
{
    public function up()
    {
        Schema::create('equipe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('estado');
            $table->string('nome');
            $table->string('cargo');
            $table->string('e_mail');
            $table->string('telefone_fixo');
            $table->string('telefone_celular');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('equipe');
    }
}
