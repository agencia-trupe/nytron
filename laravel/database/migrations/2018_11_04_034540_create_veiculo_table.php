<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeiculoTable extends Migration
{
    public function up()
    {
        Schema::create('veiculo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao');
            $table->integer('id_marca');
            $table->integer('id_linha');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('veiculo');
    }
}
