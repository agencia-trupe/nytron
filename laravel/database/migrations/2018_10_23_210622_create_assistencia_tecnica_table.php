<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistenciaTecnicaTable extends Migration
{
    public function up()
    {
        Schema::create('assistencia_tecnica', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('assistencia_tecnica');
    }
}
