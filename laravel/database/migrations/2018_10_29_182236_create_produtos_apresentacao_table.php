<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosApresentacaoTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_apresentacao', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->string('imagem');
            $table->text('kits_de_distribuicao');
            $table->text('tensionadores_e_polias');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtos_apresentacao');
    }
}
