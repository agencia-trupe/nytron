<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogosTable extends Migration
{
    public function up()
    {
        Schema::create('catalogos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem_tensionadores_e_polias');
            $table->string('arquivo_tensionadores_e_polias');
            $table->string('imagem_kits_de_distribuicao');
            $table->string('arquivo_kits_de_distribuicao');
            $table->string('arquivo_banners');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('catalogos');
    }
}
