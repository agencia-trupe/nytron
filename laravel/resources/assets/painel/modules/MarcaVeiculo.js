export default function MarcaVeiculo() {
    var refreshMultiSelect = function() {
        $('.multi-veiculos').multiselect('destroy');
        $('.multi-veiculos').multiselect({
            nonSelectedText: 'Selecione',
            allSelectedText: 'Todos',
            nSelectedText: ' selecionados',
            numberDisplayed: 4,
            buttonWidth: '100%'
        });
    };

    $('.fetch-veiculos').change(function() {
        var marcaId = $(this).val();

        if (marcaId) {
            var url = $('base').attr('href') + '/painel/marca/' + marcaId;

            $.get(url, function(data) {
                $('.multi-veiculos option').remove();
                $.each(data, function(index, value) {
                    $('.multi-veiculos').append('<option value="'+index+'">'+value+'</option>');
                });
                refreshMultiSelect();
            });
        } else {
            $('.multi-veiculos option').remove();
            refreshMultiSelect();
        }
    });
};
