import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle({
    slides: '> .banner'
});

$('.tab-handle').click(function(event) {
    event.preventDefault();

    if ($(this).hasClass('open')) {
        $(this).removeClass('open').next().slideUp();
        return;
    }

    $('.tab-handle').removeClass('open');
    $('.tab-content').slideUp();

    $(this).addClass('open').next().slideDown();
});

$('select[name=montadora]').change(function() {
    var marcaId = $(this).val();

    $('select[name=modelo_veiculo] option:not(:first-child)').remove()
    $('select[name=modelo_veiculo]').attr('disabled', true);

    if (marcaId) {
        var url = $('base').attr('href') + '/veiculos/' + marcaId;

        $.get(url, function(data) {
            $.each(data, function(index, value) {
                $('select[name=modelo_veiculo]').append('<option value="'+index+'">'+value+'</option>');
            });
            $('select[name=modelo_veiculo]').attr('disabled', false);
        });
    }
});
