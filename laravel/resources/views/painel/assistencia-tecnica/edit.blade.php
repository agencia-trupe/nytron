@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Assistência Técnica</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.assistencia-tecnica.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.assistencia-tecnica.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
