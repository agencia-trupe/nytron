@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($registro->imagem)
    <img src="{{ url('assets/img/produtos/apresentacao/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('kits_de_distribuicao', 'Kits de Distribuição') !!}
    {!! Form::textarea('kits_de_distribuicao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'produtosApresentacao']) !!}
</div>

<div class="form-group">
    {!! Form::label('tensionadores_e_polias', 'Tensionadores e Polias') !!}
    {!! Form::textarea('tensionadores_e_polias', null, ['class' => 'form-control ckeditor', 'data-editor' => 'produtosApresentacao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
