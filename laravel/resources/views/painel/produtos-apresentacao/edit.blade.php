@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Apresentação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos-apresentacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos-apresentacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
