@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Equipe /</small> Texto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.equipe-texto.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.equipe-texto.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
