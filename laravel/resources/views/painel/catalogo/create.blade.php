@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Catálogo /</small> Adicionar Produto</h2>
    </legend>

    {!! Form::open(['route' => 'painel.catalogo.store', 'files' => true]) !!}

        @include('painel.catalogo.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
