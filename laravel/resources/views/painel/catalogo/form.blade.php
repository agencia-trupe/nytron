@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::text('descricao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cd_nytron', 'Código Nytron') !!}
    {!! Form::text('cd_nytron', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cd_original', 'Código Original') !!}
    {!! Form::text('cd_original', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('aplicacao', 'Aplicação') !!}
    {!! Form::text('aplicacao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('refil', 'Refil') !!}
    {!! Form::text('refil', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('categoria', 'Categoria') !!}
    {!! Form::select('categoria[]', $categorias, isset($registro) ? $registro->categoria->pluck('id')->toArray() : null, ['class' => 'form-control multi-select', 'multiple' => true]) !!}
</div>

<div class="form-group">
    {!! Form::label('id_marca', 'Marca') !!}
    {!! Form::select('id_marca', $marcas, null, ['class' => 'form-control fetch-veiculos', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('veiculo', 'Veículo') !!}
    {!! Form::select('veiculo[]', $veiculos, isset($registro) ? $registro->veiculo->pluck('id')->toArray() : null, ['class' => 'form-control multi-veiculos multi-select', 'multiple' => true]) !!}
</div>

<div class="well form-group">
    {!! Form::label('foto1', 'Foto 1') !!}
@if($submitText == 'Alterar')
    @if($registro->foto1)
    <img src="{{ url('assets/img/catalogo/'.$registro->foto1) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width:400px">
    @endif
@endif
    {!! Form::file('foto1', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('foto2', 'Foto 2') !!}
@if($submitText == 'Alterar')
    @if($registro->foto2)
    <img src="{{ url('assets/img/catalogo/'.$registro->foto2) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width:400px">
    @endif
@endif
    {!! Form::file('foto2', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.catalogo.index') }}" class="btn btn-default btn-voltar">Voltar</a>
