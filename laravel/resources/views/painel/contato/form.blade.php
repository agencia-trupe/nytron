@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('telefone_comercial', 'Telefone Comercial') !!}
            {!! Form::text('telefone_comercial', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('endereco_comercial', 'Endereço Comercial') !!}
            {!! Form::textarea('endereco_comercial', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('telefone_fabrica', 'Telefone Fábrica') !!}
            {!! Form::text('telefone_fabrica', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('endereco_fabrica', 'Endereço Fábrica') !!}
            {!! Form::textarea('endereco_fabrica', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('facebook', 'Facebook') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
