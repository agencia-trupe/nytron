<ul class="nav navbar-nav">
    <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
	<li @if(Tools::routeIs('painel.catalogos*')) class="active" @endif>
		<a href="{{ route('painel.catalogos.index') }}">Catálogos</a>
	</li>
    <li @if(Tools::routeIs('painel.empresa*')) class="active" @endif>
        <a href="{{ route('painel.empresa.index') }}">Empresa</a>
    </li>
    <li class="dropdown @if(Tools::routeIs(['painel.produtos*', 'painel.catalogo.*', 'painel.categoria*', 'painel.linha*', 'painel.marca*', 'painel.veiculo*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Produtos
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.produtos-apresentacao*')) class="active" @endif>
                <a href="{{ route('painel.produtos-apresentacao.index') }}">Apresentação</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.catalogo.*')) class="active" @endif>
                <a href="{{ route('painel.catalogo.index') }}">Catálogo</a>
            </li>
            <li @if(Tools::routeIs('painel.categoria*')) class="active" @endif>
                <a href="{{ route('painel.categoria.index') }}">Categoria</a>
            </li>
            <li @if(Tools::routeIs('painel.linha*')) class="active" @endif>
                <a href="{{ route('painel.linha.index') }}">Linha</a>
            </li>
            <li @if(Tools::routeIs('painel.marca*')) class="active" @endif>
                <a href="{{ route('painel.marca.index') }}">Marca</a>
            </li>
            <li @if(Tools::routeIs('painel.veiculo*')) class="active" @endif>
                <a href="{{ route('painel.veiculo.index') }}">Veículo</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(Tools::routeIs(['painel.equipe*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Equipe
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.equipe-texto*')) class="active" @endif>
                <a href="{{ route('painel.equipe-texto.index') }}">Texto</a>
            </li>
            <li @if(Tools::routeIs('painel.equipe.*')) class="active" @endif>
                <a href="{{ route('painel.equipe.index') }}">Equipe</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(Tools::routeIs(['painel.assistencia-tecnica*', 'painel.tecnicos*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Assistência Técnica
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.assistencia-tecnica*')) class="active" @endif>
                <a href="{{ route('painel.assistencia-tecnica.index') }}">Assistência Técnica</a>
            </li>
            <li @if(Tools::routeIs('painel.tecnicos*')) class="active" @endif>
                <a href="{{ route('painel.tecnicos.index') }}">Técnicos</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
</ul>
