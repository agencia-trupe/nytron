@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Técnicos /</small> Editar Técnico</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.tecnicos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.tecnicos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
