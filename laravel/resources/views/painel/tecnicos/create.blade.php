@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Técnicos /</small> Adicionar Técnico</h2>
    </legend>

    {!! Form::open(['route' => 'painel.tecnicos.store', 'files' => true]) !!}

        @include('painel.tecnicos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
