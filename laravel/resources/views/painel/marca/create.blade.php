@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marca /</small> Adicionar Marca</h2>
    </legend>

    {!! Form::open(['route' => 'painel.marca.store', 'files' => true]) !!}

        @include('painel.marca.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
