@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marca /</small> Editar Marca</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.marca.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.marca.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
