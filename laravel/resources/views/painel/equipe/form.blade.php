@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('estado', 'Estado') !!}
    {!! Form::select('estado', Tools::selectEstados(), null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cargo', 'Cargo') !!}
    {!! Form::text('cargo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('e_mail', 'E-mail') !!}
    {!! Form::text('e_mail', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone_fixo', 'Telefone Fixo') !!}
    {!! Form::text('telefone_fixo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone_celular', 'Telefone Celular') !!}
    {!! Form::text('telefone_celular', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.equipe.index') }}" class="btn btn-default btn-voltar">Voltar</a>
