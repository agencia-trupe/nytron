@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_tensionadores_e_polias', 'Imagem Tensionadores e Polias') !!}
            @if($registro->imagem_tensionadores_e_polias)
            <img src="{{ url('assets/img/catalogos/'.$registro->imagem_tensionadores_e_polias) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_tensionadores_e_polias', ['class' => 'form-control']) !!}
        </div>

        <div class="well form-group">
            {!! Form::label('arquivo_tensionadores_e_polias', 'Arquivo Tensionadores e Polias') !!}
            @if($registro->arquivo_tensionadores_e_polias)
            <a href="{{ url('arquivos/'.$registro->arquivo_tensionadores_e_polias) }}" style="display:block;margin-bottom:10px" target="_blank">{{ $registro->arquivo_tensionadores_e_polias }}</a>
            @endif
            {!! Form::file('arquivo_tensionadores_e_polias', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_kits_de_distribuicao', 'Imagem Kits de Distribuição') !!}
            @if($registro->imagem_kits_de_distribuicao)
            <img src="{{ url('assets/img/catalogos/'.$registro->imagem_kits_de_distribuicao) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_kits_de_distribuicao', ['class' => 'form-control']) !!}
        </div>

        <div class="well form-group">
            {!! Form::label('arquivo_kits_de_distribuicao', 'Arquivo Kits de Distruibuição') !!}
            @if($registro->arquivo_kits_de_distribuicao)
            <a href="{{ url('arquivos/'.$registro->arquivo_kits_de_distribuicao) }}" style="display:block;margin-bottom:10px" target="_blank">{{ $registro->arquivo_kits_de_distribuicao }}</a>
            @endif
            {!! Form::file('arquivo_kits_de_distribuicao', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="well form-group">
    {!! Form::label('arquivo_banners', 'Arquivo Catálogo Geral (Banners)') !!}
    @if($registro->arquivo_banners)
    <a href="{{ url('arquivos/'.$registro->arquivo_banners) }}" style="display:block;margin-bottom:10px" target="_blank">{{ $registro->arquivo_banners }}</a>
    @endif
    {!! Form::file('arquivo_banners', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
