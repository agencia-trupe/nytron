@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Catálogos</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.catalogos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.catalogos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
