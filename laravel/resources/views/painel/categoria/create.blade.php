@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Categoria /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.categoria.store', 'files' => true]) !!}

        @include('painel.categoria.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
