@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Categoria /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.categoria.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.categoria.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
