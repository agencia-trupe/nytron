@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linha /</small> Editar Linha</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.linha.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.linha.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
