@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linha /</small> Adicionar Linha</h2>
    </legend>

    {!! Form::open(['route' => 'painel.linha.store', 'files' => true]) !!}

        @include('painel.linha.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
