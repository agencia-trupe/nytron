@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Veículo /</small> Editar Veículo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.veiculo.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.veiculo.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
