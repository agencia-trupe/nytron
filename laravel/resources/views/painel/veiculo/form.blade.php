@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::text('descricao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('id_marca', 'Marca') !!}
    {!! Form::select('id_marca', $marcas, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('id_linha', 'Linha') !!}
    {!! Form::select('id_linha', $linhas, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.veiculo.index') }}" class="btn btn-default btn-voltar">Voltar</a>
