@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Veículo /</small> Adicionar Veículo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.veiculo.store', 'files' => true]) !!}

        @include('painel.veiculo.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
