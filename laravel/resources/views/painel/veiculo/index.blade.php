@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Veículo
            <a href="{{ route('painel.veiculo.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Veículo</a>
        </h2>
    </legend>

    <div class="row">
        <div class="col-md-4">
            <div class="well">
                <form action="{{ route('painel.veiculo.index') }}" method="GET">
                    <div class="input-group">
                        <input type="text" name="filtro" class="form-control" value="{{ request('filtro') }}" placeholder="Veículo">
                        <div class="input-group-btn">
                            <input type="submit" class="btn btn-info" value="Pesquisar">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Marca</th>
                <th>Linha</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->descricao }}</td>
                <td>{{ $registro->marca->descricao }}</td>
                <td>{{ $registro->linha->descricao }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.veiculo.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.veiculo.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $registros->appends($_GET)->render() !!}
    @endif

@endsection
