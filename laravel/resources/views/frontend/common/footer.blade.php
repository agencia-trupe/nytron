    <footer>
        <div class="footer-bg">
            <div class="center">
                <div class="left">
                    <div class="marca">
                        <img src="{{ asset('assets/img/layout/nytron.png') }}" alt="">
                        <p>TENSORES, POLIAS E KITS DE DISTRIBUIÇÃO</p>
                        @if($contato->facebook)
                        <a href="{{ Tools::parseLink($contato->facebook) }}" target="_blank" class="facebook">fb.com/nytronindustria</a>
                        @endif
                    </div>
                    <nav>
                        <a href="{{ route('home') }}">&raquo; Home</a>
                        <a href="{{ route('empresa') }}">&raquo; Empresa</a>
                        <a href="{{ route('produtos') }}">&raquo; Produtos</a>
                        <a href="{{ route('equipe') }}">&raquo; Equipe</a>
                        <a href="{{ route('assistencia-tecnica') }}">&raquo; Assistência Técnica</a>
                        <a href="{{ route('contato') }}">&raquo; Contato</a>
                    </nav>
                </div>

                <div class="right">
                    <p class="telefone">{{ $contato->telefone_comercial }}</p>
                    <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                    <p class="endereco">{!! $contato->endereco_comercial !!}</p>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="center">
                <p>
                    © {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados.
                    <span>|</span>
                    <a href="http://www.trupe.net" target="_blank">Criação de Sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
