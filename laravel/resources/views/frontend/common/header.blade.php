    <div class="area-do-cliente">
        <div class="center">
            <a href="https://ebusiness.nytron.ind.br/NytronEbizWeb/" target="_blank">ÁREA DO CLIENTE</a>
        </div>
    </div>

    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">
                {{ config('app.name') }}
            </a>

            <nav id="nav-desktop">
                <a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa')) class="active" @endif>Empresa</a>
                <a href="{{ route('produtos') }}" @if(Tools::routeIs('produtos*')) class="active" @endif>Produtos</a>
                <a href="{{ route('equipe') }}" @if(Tools::routeIs('equipe')) class="active" @endif>Equipe</a>
                <a href="{{ route('assistencia-tecnica') }}" @if(Tools::routeIs('assistencia-tecnica')) class="active" @endif>Assistência Técnica</a>
                <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>Contato</a>
            </nav>

            @if($contato->facebook)
            <a href="{{ Tools::parseLink($contato->facebook) }}" class="facebook" target="_blank">facebook</a>
            @endif

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <nav id="nav-mobile">
            <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>Home</a>
            <a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa')) class="active" @endif>Empresa</a>
            <a href="{{ route('produtos') }}" @if(Tools::routeIs('produtos*')) class="active" @endif>Produtos</a>
            <a href="{{ route('equipe') }}" @if(Tools::routeIs('equipe')) class="active" @endif>Equipe</a>
            <a href="{{ route('assistencia-tecnica') }}" @if(Tools::routeIs('assistencia-tecnica')) class="active" @endif>Assistência Técnica</a>
            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>Contato</a>
        </nav>
    </header>
