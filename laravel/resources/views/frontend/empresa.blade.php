@extends('frontend.common.template')

@section('content')

    <div class="conteudo empresa">
        <div class="center">
            <img src="{{ asset('assets/img/empresa/'.$empresa->imagem) }}" alt="">
            <div class="texto">{!! $empresa->texto !!}</div>
        </div>
    </div>

@endsection
