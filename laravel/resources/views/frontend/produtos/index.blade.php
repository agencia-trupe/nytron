@extends('frontend.common.template')

@section('content')

    <div class="conteudo produtos">
        <div class="center">
            @include('frontend.produtos._busca')

            <div class="apresentacao">
                {!! $apresentacao->texto !!}

                <img src="{{ asset('assets/img/produtos/apresentacao/'.$apresentacao->imagem) }}" alt="">

                <div class="kits">
                    {!! $apresentacao->kits_de_distribuicao !!}
                    <a href="{{ url('arquivos/'.$catalogos->arquivo_kits_de_distribuicao) }}" target="_blank">Consulte o catálogo Nytron</a>
                </div>

                <div class="tensionadores">
                    {!! $apresentacao->tensionadores_e_polias !!}
                    <a href="{{ url('arquivos/'.$catalogos->arquivo_tensionadores_e_polias) }}" target="_blank">Consulte o catálogo Nytron</a>
                </div>
            </div>
        </div>

        <div class="divider"></div>

        <div class="center">
            <div class="produtos-fullwidth">
                @foreach($produtos as $produto)
                    @include('frontend.produtos._thumb', compact('produto'))
                @endforeach
            </div>
        </div>
    </div>

@endsection
