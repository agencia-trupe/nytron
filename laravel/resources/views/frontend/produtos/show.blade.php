@extends('frontend.common.template')

@section('content')

    <div class="conteudo produtos">
        <div class="center">
            @include('frontend.produtos._busca')

            <div class="detalhes">
                @if($produto->foto1)
                <div class="capa">
                    <img src="{{ asset('assets/img/catalogo/'.$produto->foto1) }}" alt="">
                </div>
                @endif

                <h1>{{ $produto->descricao }}</h1>
                <p class="cod-nytron">
                    Código Nytron:
                    <span>{{ $produto->cd_nytron }}</span>
                </p>

                @if($produto->cd_original)
                <p class="cod-original">
                    Código Original:
                    <span>{{ $produto->cd_original }}</span>
                </p>
                @endif

                @if($produto->aplicacao)
                <p class="aplicacao">
                    <span>Aplicação:</span>
                    <?php $aplicacoes = explode('/', $produto->aplicacao); ?>
                    {!! join('<br>', $aplicacoes) !!}
                </p>
                @endif

                @if($produto->refil)
                <p class="refil">
                    <span>Refil:</span>
                    {{ $produto->refil }}
                </p>
                @endif

                @if($produto->foto2)
                <div class="imagens">
                    <img src="{{ asset('assets/img/catalogo/'.$produto->foto2) }}" alt="">
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection
