<div class="busca">
    <p>BUSCA AVANÇADA</p>

    <form action="{{ route('produtos.busca') }}">
        <input type="text" name="geral" placeholder="Busca geral" value="{{ request('geral') }}">
        <input type="text" name="cd_original" placeholder="Código original" value="{{ request('cd_original') }}">
        <input type="text" name="cd_nytron" placeholder="Código Nytron" value="{{ request('cd_nytron') }}">
        <input type="submit" value="BUSCAR">
    </form>

    <form action="{{ route('produtos.busca') }}">
        <label>
            Tipo do Veículo
            <select name="tipo_veiculo">
                <option value="">Selecione...</option>
                @foreach($linhas as $id => $titulo)
                <option value="{{ $id }}" @if(request('tipo_veiculo') == $id) selected @endif>{{ $titulo }}</option>
                @endforeach
            </select>
        </label>
        <label>
            Montadora
            <select name="montadora">
                <option value="">Selecione...</option>
                @foreach($marcas as $id => $titulo)
                <option value="{{ $id }}" @if(request('montadora') == $id) selected @endif>{{ $titulo }}</option>
                @endforeach
            </select>
        </label>
        <label>
            Modelo do Veículo
            <select name="modelo_veiculo" @if(!isset($veiculos)) disabled @endif>
                <option value="">Selecione...</option>
                @if(isset($veiculos))
                    @foreach($veiculos as $id => $titulo)
                    <option value="{{ $id }}" @if(request('modelo_veiculo') == $id) selected @endif>{{ $titulo }}</option>
                    @endforeach
                @endif
            </select>
        </label>
        <label>
            Produto
            <select name="produto">
                <option value="">Selecione...</option>
                @foreach($categorias as $id => $titulo)
                <option value="{{ $id }}" @if(request('produto') == $id) selected @endif>{{ $titulo }}</option>
                @endforeach
            </select>
        </label>
        <input type="submit" value="BUSCAR">
    </form>
</div>
