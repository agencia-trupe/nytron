<a href="{{ route('produtos.show', $produto->id) }}" class="produto-thumb">
    @if($produto->foto1)
    <img src="{{ asset('assets/img/catalogo/'.$produto->foto1) }}" alt="">
    @else
    <div class="placeholder"></div>
    @endif
    <span class="titulo">{{ $produto->descricao }}</span>
    <span class="codigo">{{ $produto->cd_nytron }}</span>
</a>
