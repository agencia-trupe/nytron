@extends('frontend.common.template')

@section('content')

    <div class="conteudo produtos">
        <div class="center">
            @include('frontend.produtos._busca')

            @if(count($produtos))
            <div class="resultados-lateral">
                @foreach($produtos->take(6) as $produto)
                    @include('frontend.produtos._thumb', compact('produto'))
                @endforeach
            </div>

            <div class="produtos-fullwidth">
                @foreach($produtos->slice(6) as $produto)
                    @include('frontend.produtos._thumb', compact('produto'))
                @endforeach
            </div>
            @else
            <div class="detalhes">
                <p>Nenhum resultado encontrado.</p>
            </div>
            @endif
        </div>
    </div>

@endsection
