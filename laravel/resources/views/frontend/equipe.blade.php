@extends('frontend.common.template')

@section('content')

    <div class="conteudo equipe">
        <div class="center">
            <div class="busca">
                <p>LOCALIZE NOSSA EQUIPE</p>
                <form action="{{ route('equipe') }}" method="GET">
                    <select name="busca">
                        <option value="">Selecione seu estado</option>
                        @foreach(Tools::selectEstados() as $uf => $estado)
                        <option value="{{ $uf }}" @if(strtoupper(request('busca')) == $uf) selected @endif>{{ $estado }}</option>
                        @endforeach
                    </select>
                    <input type="submit" value="BUSCAR">
                    <img src="{{ asset('assets/img/layout/mapa.png') }}" alt="">
                </form>
            </div>

            @if(! request('busca'))
            <div class="texto">
                {!! $texto->texto !!}

                <div class="estados">
                    @foreach($equipe as $estado => $contatos)
                    <div class="tab">
                        <a href="#" class="tab-handle">{{ Tools::selectEstados()[$estado] }} ({{ $estado }})</a>
                        <div class="tab-content">
                            @foreach($contatos as $contato)
                            <div class="equipe-contato">
                                @if($contato->cargo)
                                <div class="cargo">{{ $contato->cargo }}</div>
                                @endif

                                <p>
                                    <span class="nome">{{ $contato->nome }}</span>
                                    @if($contato->e_mail)
                                    <a href="mailto:{{ $contato->e_mail }}" class="email">{{ $contato->e_mail }}</a>
                                    @endif
                                </p>

                                <p>
                                    @if($contato->telefone_celular)
                                    <span class="celular">{{ $contato->telefone_celular }}</span>
                                    @endif
                                    @if($contato->telefone_fixo)
                                    <span class="telefone">{{ $contato->telefone_fixo }}</span>
                                    @endif
                                </p>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @else
            <div class="texto">
                <h2>{{ Tools::selectEstados()[strtoupper(request('busca'))] }} ({{ strtoupper(request('busca')) }})</h2>

                @if(!count($equipe))
                <p>Nenhum registro encontrado.</p>
                @endif

                @foreach($equipe as $contato)
                <div class="equipe-contato">
                    @if($contato->cargo)
                    <div class="cargo">{{ $contato->cargo }}</div>
                    @endif

                    <p>
                        <span class="nome">{{ $contato->nome }}</span>
                        @if($contato->e_mail)
                        <a href="mailto:{{ $contato->e_mail }}" class="email">{{ $contato->e_mail }}</a>
                        @endif
                    </p>

                    <p>
                        @if($contato->telefone_celular)
                        <span class="celular">{{ $contato->telefone_celular }}</span>
                        @endif
                        @if($contato->telefone_fixo)
                        <span class="telefone">{{ $contato->telefone_fixo }}</span>
                        @endif
                    </p>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>

@endsection
