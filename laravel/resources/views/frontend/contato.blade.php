@extends('frontend.common.template')

@section('content')

    <div class="conteudo contato">
        <div class="center">
            <div class="informacoes">
                <p>
                    <span class="tipo">COMERCIAL</span>
                    <span class="telefone">{{ $contato->telefone_comercial }}</span>
                    <span class="endereco">{!! $contato->endereco_comercial !!}</span>
                </p>
                <p>
                    <span class="tipo">FÁBRICA</span>
                    <span class="telefone">{{ $contato->telefone_fabrica }}</span>
                    <span class="endereco">{!! $contato->endereco_fabrica !!}</span>
                </p>
            </div>

            <form action="{{ route('contato.post') }}" method="POST">
                {!! csrf_field() !!}

                <p>FALE CONOSCO</p>

                @if(session('enviado'))
                <div class="flash enviado">Mensagem enviada com sucesso!</div>
                @endif
                @if($errors->any())
                <div class="flash erro">Preencha todos os campos corretamente.</div>
                @endif

                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <input type="submit" value="ENVIAR">
            </form>
        </div>

        <div class="mapa">{!! $contato->google_maps !!}</div>
    </div>

@endsection
