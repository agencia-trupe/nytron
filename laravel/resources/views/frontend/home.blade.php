@extends('frontend.common.template')

@section('content')

    <div class="conteudo home">
        <div class="center">
            <div class="banners">
                @foreach($banners as $banner)
                <div class="banner" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                    <div class="texto">
                        {!! $banner->texto !!}

                        @if($catalogos->arquivo_banners)
                        <a href="{{ url('arquivos/'.$catalogos->arquivo_banners) }}" class="btn-catalogo" target="_blank">download do catálogo</a>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>

            <div class="destaques">
                <h2>
                    <span>CONFIRA ALGUNS DOS NOSSOS PRODUTOS</span>
                </h2>
                <div class="lista-destaques">
                    @foreach($produtos as $produto)
                        @include('frontend.produtos._thumb', compact('produto'))
                    @endforeach
                </div>
            </div>
        </div>

        <div class="catalogos">
            <div class="center">
                <div class="catalogo">
                    <p>FAÇA O DOWNLOAD DO CATÁLOGO COMPLETO</p>
                    <h2>TENSIONADORES E POLIAS</h2>
                    <a href="{{ url('arquivos/'.$catalogos->arquivo_tensionadores_e_polias) }}" class="box" target="_blank">
                        <img src="{{ asset('assets/img/catalogos/'.$catalogos->imagem_tensionadores_e_polias) }}" alt="">
                        <span>DOWNLOAD</span>
                    </a>
                </div>

                <div class="catalogo">
                    <p>FAÇA O DOWNLOAD DO CATÁLOGO COMPLETO</p>
                    <h2>KITS DE DISTRIBUIÇÃO</h2>
                    <a href="{{ url('arquivos/'.$catalogos->arquivo_kits_de_distribuicao) }}" class="box" target="_blank">
                        <img src="{{ asset('assets/img/catalogos/'.$catalogos->imagem_kits_de_distribuicao) }}" alt="">
                        <span>DOWNLOAD</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection
