@extends('frontend.common.template')

@section('content')

    <div class="conteudo assistencia-tecnica">
        <div class="center">
            <img src="{{ asset('assets/img/assistencia-tecnica/'.$assistenciaTecnica->imagem) }}" alt="">

            <div class="texto">
                <div>{!! $assistenciaTecnica->texto !!}</div>

                <div class="tecnicos">
                    @foreach($tecnicos as $tecnico)
                    <a href="mailto:{{ $tecnico->e_mail }}">
                        {{ $tecnico->nome }}
                        <span>{{ $tecnico->e_mail }}</span>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
